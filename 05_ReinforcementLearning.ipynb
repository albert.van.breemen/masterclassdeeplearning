{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\">\n",
    "<a href=\"https://vbti.nl\"><img src=\"images/vbti_logo.png\" width=\"400\"></a>\n",
    "</div>\n",
    "\n",
    "# Reinforcement Learning\n",
    "This notebook supports the 'Reinforcement Learning' chapter of the [1-day masterclass \"Deep Learning\"](https://aiblog.nl/masterclass-deep-learning). It is not ment as a full course on deep learning, but rather gives you a flavor of the topic. For an in-depth AI training or consultancy please contact [VBTI](https://vbti.nl). \n",
    "\n",
    "Reinforcement Learning is an AI technique to learn an optimal sequence of actions. During the masterclass details of the action-value method, Q-learning, exploration/exploitation, discount factor and Deep Reinforcement Learning are explained. In this notebook you will build and train a RL agent that needs to optimize planning taxi trips.\n",
    "\n",
    "<div align=\"center\">\n",
    "<a href=\"https://aiblog.nl/masterclass-deep-learning\"><img src=\"images/rl.png\" width=\"400\"></a>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import some default libaries\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Environment preparation\n",
    "In this example the [gym tool](https://gym.openai.com) is used to create a simulation environment in which an agent needs to learn a task. The environment used is called 'Taxi-v2'. In this toy environment the agents needs to drive a taxi to pick up and drop passengers. \n",
    "\n",
    "> There are 4 locations (labeled by different letters) and your job is to pick up the passenger at one location and drop him off in another. You receive +20 points for a successful dropoff, and lose 1 point for every timestep it takes. There is also a 10 point penalty for illegal pick-up and drop-off actions. (['Taxi-v2'](https://gym.openai.com/envs/Taxi-v2/))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of actions : Discrete(6)\n",
      "Number of states  : Discrete(500)\n",
      "Initial state     : 223\n"
     ]
    }
   ],
   "source": [
    "# import and create the simulation environment\n",
    "import gym\n",
    "env = gym.make(\"Taxi-v2\")\n",
    "\n",
    "print(\"Number of actions : {}\".format(env.action_space))\n",
    "print(\"Number of states  : {}\".format(env.observation_space))\n",
    "\n",
    "initial_state = env.reset()\n",
    "print(\"Initial state     : {}\".format(initial_state))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+---------+\n",
      "|\u001b[34;1mR\u001b[0m: | : :G|\n",
      "| : : : : |\n",
      "| :\u001b[43m \u001b[0m: : : |\n",
      "| | : | : |\n",
      "|Y| : |\u001b[35mB\u001b[0m: |\n",
      "+---------+\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# render environment\n",
    "env.render()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Build Q-learning Agent\n",
    "In the masterclass the Q-learning technique is explained. Following this methods, an agent learns to evaluate a state-action combination by using the following rule:\n",
    "\n",
    "$$\n",
    "Q(s_t,a_t) \\leftarrow Q(s_t,a_t) + \\alpha \\left[ R_{t+1} +\\gamma \\max\\limits_{a} Q(s_{t+1},a) - Q(s_t,a_t) \\right]\n",
    "$$\n",
    "\n",
    "The code below implements this rule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from rl import Agent\n",
    "\n",
    "class QLearningAgent(Agent):\n",
    "    def __init__(self, n_states, n_actions, epsilon=0.1, gamma=0.9, alpha=0.1):\n",
    "        Agent.__init__(self)\n",
    "        \n",
    "        self.n_states = n_states\n",
    "        self.n_actions = n_actions\n",
    "\n",
    "        self.epsilon = epsilon\n",
    "        self.gamma = gamma\n",
    "        self.alpha = alpha\n",
    "        \n",
    "        self.learn = True\n",
    "                \n",
    "        self.reset()\n",
    "        \n",
    "    def reset(self):\n",
    "        Agent.reset(self)                \n",
    "        self.Q = np.zeros((self.n_states, self.n_actions))\n",
    "                \n",
    "    def get_action(self, state):\n",
    "        # e-greedy\n",
    "        if self.learn & (np.random.random()<self.epsilon): # explore\n",
    "            a = np.random.randint(self.n_actions)\n",
    "        else: # exploit\n",
    "            a = np.argmax(self.Q[state,:])\n",
    "        return a\n",
    "    \n",
    "    def update(self, state, action, next_state, reward):\n",
    "        if self.learn:\n",
    "            self.Q[state, action] = self.Q[state, action] + self.alpha * (reward + \n",
    "                                                                          self.gamma * np.max(self.Q[next_state,:]) -\n",
    "                                                                          self.Q[state, action])\n",
    "        Agent.update(self, state, action, next_state, reward)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a Q-learning agent\n",
    "agent_Q = QLearningAgent(n_states=500, n_actions=6, epsilon=0.1, alpha=0.1, gamma=1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Train agent\n",
    "To train a Q-learning agent it needs to try to drive the taxi many times. A maximum duration of 200 steps will be allowed to pick up and drop a passenger. \n",
    "\n",
    "First, let's see how well the agents performance without learning. The average performance over 10 experimens is calculated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+---------+\n",
      "|R: | : :\u001b[34;1mG\u001b[0m|\n",
      "| : : : : |\n",
      "| : : : : |\n",
      "| | : | : |\n",
      "|Y| : |\u001b[35mB\u001b[0m:\u001b[43m \u001b[0m|\n",
      "+---------+\n",
      "  (South)\n",
      "\n",
      "Average reward       : -200.0\n",
      "Average #penalties   : 0.0\n",
      "Average #steps       : 200.0\n",
      "Average #reward/step : -1.0\n"
     ]
    }
   ],
   "source": [
    "# calculate performance of agent\n",
    "# Note: sometimes the agent does not move but the simulation does run. Be patient! ;)\n",
    "from rl import run_experiment\n",
    "\n",
    "agent_Q.learn = False\n",
    "run_experiment(env, agent_Q, n_experiments=10, max_steps=200, render=True)\n",
    "agent_Q.learn = True"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most likely, the agent will performance very bad (average reward: -200, average number of steps: 200). Therefore 20.000 experiments are carried out during which the agent learns to carry out its job at best."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Run experiment : 0 / 20000\n",
      "Run experiment : 10000 / 20000\n",
      "Done.\n",
      "\n",
      "Average reward       : -1.6832\n",
      "Average #penalties   : 0.6031\n",
      "Average #steps       : 17.10515\n",
      "Average #reward/step : 0.28073684369620894\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXoAAAEKCAYAAAAcgp5RAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvhp/UCwAAIABJREFUeJzt3Xd8FOX2+PHPoQgoSEdpElBAUSGBgCIWEAUUFSyo2FBU1Kv3p14b6FW5tpuv/VouiIqiYEcEUa4URZqUAKGEGiBAAoRQQmgBkpzfHzMJm7pJdjdLds/79coru8/MPHN2dvbsM8/MziOqijHGmNBVKdgBGGOMCSxL9MYYE+Is0RtjTIizRG+MMSHOEr0xxoQ4S/TGGBPiLNEbY0yIs0RvjDEhzhK9McaEuCrBDgCgQYMGGhEREewwjDGmQlm8ePEuVW3obb4TItFHREQQGxsb7DCMMaZCEZHNJZnPum6MMSbEWaI3xpgQZ4neGGNC3AnRR2+MqbiOHTtGUlISGRkZwQ4lZFWvXp1mzZpRtWrVMi1vid4Y45OkpCRq1apFREQEIhLscEKOqrJ7926SkpJo2bJlmerw2nUjIs1F5A8RWS0i8SLyqFteT0Smich6939dt1xE5D0RSRCR5SLSsUyRGWMqhIyMDOrXr29JPkBEhPr16/t0xFSSPvpM4AlVPQe4EHhYRNoBQ4EZqtoamOE+B7gKaO3+DQFGlDk6Y0yFYEk+sHzdvl67blR1O7DdfbxfRFYDTYF+QHd3tjHATOAZt/wLdcYonC8idUSksVuPX+3Yl8GF/55R5PSNr11NpUq2AxpjwluprroRkQggClgAnJaTvN3/jdzZmgJbPRZLcsvy1zVERGJFJDY1NbX0kQNxW9OKnf7g2MVlqtcYU3ENGzaMmTNn8tNPPxETE1PkfDNnzmTevHnlGFnwlDjRi0hNYDzwmKqmFzdrIWUFRiBX1VGqGq2q0Q0bev0Fb6HOalSz2Olb9hwqU73GmIprwYIFXHDBBfz5559ccsklRc4XTom+RFfdiEhVnCQ/TlV/dItTcrpkRKQxsNMtTwKaeyzeDNjmr4DzKvD9kYf1GxoTPp566il+++03Nm3aRNeuXdmwYQMzZszgpptuok6dOowcOZIqVarQrl07YmJiGDlyJJUrV2bs2LG8//77nH322Tz44INs2bIFgHfffZdu3boxfPhwNmzYQHJyMlu3buXpp5/m/vvvZ/v27dxyyy2kp6eTmZnJiBEjiv1iCSaviV6cbPkpsFpV3/aYNAkYBMS4/yd6lD8iIt8AFwD7AtE/b4w58fzr53hWbSvugL/02jU5lRevPdfrfG+88QYDBgzgyy+/5O2336Z79+7MnTsXgCZNmrBp0yaqVatGWloaderU4cEHH6RmzZo8+eSTANx22208/vjjXHzxxWzZsoXevXuzevVqAJYvX878+fM5ePAgUVFR9O3bl6+//prevXvz3HPPkZWVxaFDJ24PQkla9N2AO4EVIhLnlj2Lk+C/E5F7gS3AAHfar8DVQAJwCLjHrxF70OIb9IX2IRljQtfSpUuJjIxkzZo1tGvXLre8ffv23H777fTv35/+/fsXuuz06dNZtWpV7vP09HT2798PQL9+/ahRowY1atSgR48eLFy4kM6dOzN48GCOHTtG//79iYyMDOyL80FJrrqZQ9E5s2ch8yvwsI9xGWMqoJK0vAMhLi6Ou+++m6SkJBo0aMChQ4dQVSIjI/nrr7/45ZdfmDVrFpMmTeLll18mPj6+QB3Z2dn89ddf1KhRo8C0/N3AIsKll17KrFmz+OWXX7jzzjt56qmnuOuuuwL2Gn1Roe9146VBj3XRGxMeIiMjiYuLo02bNqxatYrLL7+c3377jbi4OKpVq8bWrVvp0aMHr7/+OmlpaRw4cIBatWrlttgBevXqxQcffJD7PC4uLvfxxIkTycjIYPfu3cycOZPOnTuzefNmGjVqxP3338+9997LkiVLyvU1l0bFTvTeMr0xJmykpqZSt25dKlWqlKfrJisrizvuuIPzzz+fqKgoHn/8cerUqcO1117LhAkTiIyMZPbs2bz33nvExsbSvn172rVrx8iRI3Pr7tKlC3379uXCCy/k+eefp0mTJsycOZPIyEiioqIYP348jz76aLBeulchfa+by9qU7bJNY0zF07BhQ3755RcA5s+fn1tetWpV5syZU2D+Nm3asHz58jxl3377baF1t2nThlGjRuUpGzRoEIMGDfI17HJRsVv0Xjpv6tesVk6RGGPMiSukW/TGGOOr4cOHBzsEn1XsFr310RtjjFcVOtEbY4zxrkInem8t+jXb/fsLPWOMqYgqdKL35vvFScEOwRhjgi6kE70xJvyU9DbF/ta9e3diY2MBeO211/JMu+iii8otjsJYojfGhJSS3qY4kPIn+mDfDrlCJ3pv19EbY8LHU089Rfv27Vm0aBFdu3blk08+4aGHHuKll16ie/fuPPbYY1x00UWcd955LFy4EICDBw8yePBgOnfuTFRUFBMnOjfh/fzzz7nhhhvo06cPrVu35umnn85dz0MPPUR0dDTnnnsuL774YoE4hg4dyuHDh4mMjOT2228HoGbN42NnvPHGG3Tu3Jn27dvnLn/w4EH69u1Lhw4dOO+884r84VZZ2XX0xhj/mTIUdqzwb52nnw9Xee+CKe42xb///jsHDx5k3rx5zJo1i8GDB7Ny5UpeffVVLr/8ckaPHk1aWhpdunThiiuuAJx73SxdupRq1arRtm1b/v73v9O8eXNeffVV6tWrR1ZWFj179mT58uW0b98+N46YmBg++OCDPPfKyTF16lTWr1/PwoULUVWuu+46Zs2aRWpqKk2aNMn9Ze++ffv8seVyVewWvTXojTEeirpNMcDAgQMBuPTSS0lPTyctLY2pU6cSExNDZGQk3bt3JyMjI3fgkZ49e1K7dm2qV69Ou3bt2Lx5MwDfffcdHTt2JCoqivj4+Dy3NvZm6tSpTJ06laioKDp27MiaNWtYv349559/PtOnT+eZZ55h9uzZ1K5d209bxGEtemOM/5Sg5R0I3m5TDIXfalhVGT9+PG3bts0zbcGCBVSrdvwWKpUrVyYzM5NNmzbx5ptvsmjRIurWrcvdd99NRkZGieNUVYYNG8YDDzxQYNrixYv59ddfGTZsGL169eKFF14ozSYoltcWvYiMFpGdIrLSo+xbEYlz/xJzBiQRkQgROewxbWTRNRtjjH8Ud5vinPvL5/R7z5kzh9q1a1O7dm169+7N+++/j7rdA0uXLi12Penp6ZxyyinUrl2blJQUpkyZUuh8VatW5dixYwXKe/fuzejRozlw4AAAycnJ7Ny5k23btnHyySdzxx138OSTT/r9lscladF/DnwAfJFToKq35DwWkbcAzw6lDap64g61YowJSUXdpjhH3bp1ueiii0hPT2f06NEAPP/88zz22GO0b98eVSUiIoLJkycXuY4OHToQFRXFueeeS6tWrejWrVuh8w0ZMoT27dvTsWNHxo0bl1veq1cvVq9eTdeuXQHnJO3YsWNJSEjgqaeeolKlSlStWpURI0b4ujnyEC1BR7eIRACTVfW8fOWCM4zg5aq6vqj5vImOjtac609LY0XSPq79oODtRz0lxvQtdb3GmJJbvXo155xzTrDDKFb37t158803iY6ODnYoZVbYdhaRxarq9UX5ejL2EiBFVdd7lLUUkaUi8qeInJhDohtjTBjx9WTsQOBrj+fbgTNUdbeIdAJ+EpFzVbXATWdEZAgwBOCMM84o08rtOnpjTEnMnDkz2CEEVZlb9CJSBbgByL2yX1WPqOpu9/FiYAPQprDlVXWUqkaranTDhjYSlDEVWUm6gE3Z+bp9fem6uQJYo6q5dw4TkYYiUtl93ApoDWz0KcJi2L5lTPBVr16d3bt3W7IPEFVl9+7dVK9evcx1eO26EZGvge5AAxFJAl5U1U+BW8nbbQNwKfCSiGQCWcCDqrqnzNEZY054zZo1IykpidTU1GCHErKqV69Os2bNyry810SvqgOLKL+7kLLxwPgyR1NK1n4wJviqVq1Ky5Ytgx2GKUaFvgWCMcYY70I+0f9n+nrvMxljTAgL+UT/zvR1wQ7BGGOCqkInejvLb4wx3lXoRG+MMca7Cp3orT1vjDHeVehEb4wxxrsKnehL2kU/MS45sIEYY8wJrEIn+pJ23vz71zUBjsMYY05cFTzRG2OM8aZCJ3q7utIYY7yr0IneGGOMdxU60VuD3hhjvKvQid4YY4x3FTrRWx+9McZ4V6ETfUmJBDsCY4wJHq+JXkRGi8hOEVnpUTZcRJJFJM79u9pj2jARSRCRtSLSO1CBA9Sq7uvY5sYYE/pK0qL/HOhTSPk7qhrp/v0KICLtcIYYPNdd5r85Y8gGwjmNTw1U1cYYEzK8JnpVnQWUdNzXfsA3qnpEVTcBCUAXH+IzxhjjI1/66B8RkeVu105dt6wpsNVjniS3zBhjTJCUNdGPAM4EIoHtwFtueWGnPQu9NkZEhohIrIjE2ujxxhgTOGVK9KqaoqpZqpoNfMzx7pkkoLnHrM2AbUXUMUpVo1U1umHDhmUJo8TsohtjTDgrU6IXkcYeT68Hcq7ImQTcKiLVRKQl0BpY6FuIxhhjfOH1+kQR+RroDjQQkSTgRaC7iETidMskAg8AqGq8iHwHrAIygYdVNSswoRtjjCkJr4leVQcWUvxpMfO/CrzqS1DGGGP8Jyx+GWuMMeHMEr0xxoQ4S/TGGBPiwiLRi93VzBgTxsIi0RtjTDizRG+MMSHOEr0xxoQ4S/TGGBPiwiLR7z10NNghGGNM0IRFoj901O7CYIwJX2GR6I0xJpxZojfGmBBnid4YY0KcJXpjjAlxluiNMSbEWaI3xpgQ5zXRi8hoEdkpIis9yt4QkTUislxEJohIHbc8QkQOi0ic+zcykMEbY4zxriQt+s+BPvnKpgHnqWp7YB0wzGPaBlWNdP8e9E+YxhhjysprolfVWcCefGVTVTXTfTofaBaA2IwxxviBP/roBwNTPJ63FJGlIvKniFxS1EIiMkREYkUkNjU11Q9hGGOMKYxPiV5EngMygXFu0XbgDFWNAv4BfCUipxa2rKqOUtVoVY1u2LChL2EYY4wpRpkTvYgMAq4BbldVBVDVI6q62328GNgAtPFHoMYYY8qmTIleRPoAzwDXqeohj/KGIlLZfdwKaA1s9EegxhhjyqaKtxlE5GugO9BARJKAF3GusqkGTHPHY53vXmFzKfCSiGQCWcCDqrqn0IqNMcaUC6+JXlUHFlL8aRHzjgfG+xqUMcYY/7FfxhpjTIizRG+MMSHOEr0xxoQ4S/TGGBPiLNEbY0yIC5tEv2nXwWCHYIwxQRE2if5IZlawQzDGmKAIm0RvjDHhyhK9McaEOEv0xhgT4sIq0asqY+YlknboaLBDMcaYchNWiT5uaxovTorn6R+WBzsUY4wpN2GV6I9kZgOQdvhYkCMxxpjyE1aJ3hhjwlHYJPojx7KDHYIxxgRFiRK9iIwWkZ0istKjrJ6ITBOR9e7/um65iMh7IpIgIstFpGOggi+NT+ZsCnYIxhgTFCVt0X8O9MlXNhSYoaqtgRnuc4CrcIYQbA0MAUb4HqbvMo7ZL2ONMeGpRIleVWcB+YcE7AeMcR+PAfp7lH+hjvlAHRFp7I9gjTHGlJ4vffSnqep2APd/I7e8KbDVY74kt8wYY0wQBOJkrBRSpgVmEhkiIrEiEpuamhqAMIwxxoBviT4lp0vG/b/TLU8CmnvM1wzYln9hVR2lqtGqGt2wYUMfwjDGGFMcXxL9JGCQ+3gQMNGj/C736psLgX05XTzBVNhhhjHGhIOSXl75NfAX0FZEkkTkXiAGuFJE1gNXus8BfgU2AgnAx8Df/B51GUxdlULS3sPBDsMYY8pdlZLMpKoDi5jUs5B5FXjYl6AC5cnvlwU7BGOMKXdh88tYT9aNY4wJJ2GZ6I0xJpyEZaJfsGkPX/6VGOwwjDGmXIRlogd4fmJ8sEMwxphyEbaJ3hhjwoUlemOMCXGW6I0xJsRZojfGmBBnid4YY0KcJXpjjAlxluiNMSbEWaI3xpgQZ4neGGNCnCV6Y4wJcZbojTEmxFmiN8aYEFeigUcKIyJtgW89iloBLwB1gPuBnBG/n1XVX8scoTHGGJ+UOdGr6logEkBEKgPJwATgHuAdVX3TLxEaY4zxib+6bnoCG1R1s5/qM8YY4yf+SvS3Al97PH9ERJaLyGgRqeundRhjjCkDnxO9iJwEXAd87xaNAM7E6dbZDrxVxHJDRCRWRGJTU1MLm8UYY4wf+KNFfxWwRFVTAFQ1RVWzVDUb+BjoUthCqjpKVaNVNbphw4Z+CMMYY0xh/JHoB+LRbSMijT2mXQ+s9MM6jDHGlFGZr7oBEJGTgSuBBzyKXxeRSECBxHzTjDHGlDOfEr2qHgLq5yu706eIytE3C7dwa5czgh2GMcYEVFj/MvajWRuDHYIxxgRcWCd6Y4wJB5bojTEmxIV1opdgB2CMMeUgrBO9McaEA0v0xhgT4sI70VvfjTEmDIR3ojfGmDBgid4YY0JcWCd667kxxoSDsE70xhgTDizRG2NMiAvrRC9inTfGmNAX1oneGGPCgSV6Y4wJcWGd6K3jxhgTDnwaeARARBKB/UAWkKmq0SJSD/gWiMAZZepmVd3r67qMMcaUnr9a9D1UNVJVo93nQ4EZqtoamOE+P6Elpx0OdgjGGBMQgeq66QeMcR+PAfoHaD0+ybnoZvqqFLrF/M70VSnBDcgYYwLAH4legakislhEhrhlp6nqdgD3f6P8C4nIEBGJFZHY1NRUP4RRdsuT9wGwctu+oMZhjDGB4HMfPdBNVbeJSCNgmoisKclCqjoKGAUQHR2tfoij1MROxxpjwoDPLXpV3eb+3wlMALoAKSLSGMD9v9PX9QRC/t9LaVC+bowxJrB8SvQicoqI1Mp5DPQCVgKTgEHubIOAib6sJ1BS0jPYe/CoteuNMSHN1xb9acAcEVkGLAR+UdX/ATHAlSKyHrjSfX7C2XvoGB1fmRbsMIwxJqB86qNX1Y1Ah0LKdwM9fam7pJpJKge0OmnUKtPy1l1jjAl1/jgZG1Rzqj1KlgpnHhnnc12W840xoahi3wJhp3OBT2XxLUXbTSyNMaGsYif6Hcv9Uo113xhjQlnFTvR2vYwxxnhVwRO9f1jXjTEmlFXsRG8Z2hhjvKrYid7frLPeGBOCLNFj97wxxoS2ip3orevGGGO8qtiJ3k9dLQs27Xaq80ttxhhzYqnYid5PLfp5G3b7pR5jjDkRVexEX05+i9/B4aNZwQ7DGGPKxBK9hxXJ+0jYuT9P2crkfTzw5WKufm82W/ccClJkxhhTdiGT6CuR7XMdM9emcsXbs/KU7Tt8DIBNuw5yyet/+LwOY4wpbxU80Qfmqps563eRtNdpvdul9RWPqjJhaRIZx6y7zRio8In+uG6VVhIh2/1S1x2fLqDHmzMB0HzX4hw8kslPS5P9sp5gWbx5D6u3pwc7jICZt2E3j3+7jNd+XV2u643fto8lW/aW6zoripXJ+4jbmuaXujbvPsjs9al+qStclDnRi0hzEflDRFaLSLyIPOqWDxeRZBGJc/+u9l+4BYLIffjlSTHMrPaE36o+luUk+Pwt+ud/Wslj38ZV6A/0jSP+4qr/zC52nnkbdrE+ZX+x8wRTxrEsvl20BS3kkGt/htPdtmNfRrnG1Pe9Odzw33lkZ9thYH7XvD+H/h/O9Utdl70xkzs/XeiXusKFLy36TOAJVT0HuBB4WETaudPeUdVI9+9Xn6MshYb4NwFn50skOZdiHjqSt1tg9fZ05m3YVWxdx7KyGTt/M1n5EsG8hF1Bb2HPWJ3C5t0H+d/KHSSnHea2jxdw5TuzCp33aGY24xYUfB3l6e1p63hm/Ap+i08pME3cBoBndOkZx/g+dmu5xPZTnPcjvuVJaSzevMfv654Yl8yeg0dzn+85eJQJS5NKXY+q8t2irRw4kgnA/I27WZm8z29xBtqUFdvZvu9wsMM4YZR5hClV3Q5sdx/vF5HVQFN/BVZWY0/6N72Pvu63+vKnsqNZ2W553ik5LeTEmL55yrelHWbJlr1c074JH8/eyOv/W0slEW674IzceW77ZAEAc57pQbO6J5c6xkNHM3luwkru6tqCqDPq5pmWmZXNVwu3cFuXM6hSuejv9XvHxCLiHME0qFmt2PV99OcG3pq2jqqVKnFz5+aljteb+Rt3U6NqZTo0r1NgWkp6BvM37mbX/iOA05WWX6WcRO/xJT1s/Ap+WbGdtqfXon2zgvUWZXlSGoeOZnFhq/oAzE3YRZ2Tq3Juk9pFLpPunsAvznUfOK3b/PtLSS3dspfMbKVzRL3csu37DvPoN3F0iajHN0MuZNyCzUxato1FiXvpdEY9zqhf8n1ryZa9PD1+OaNmb+Slfudy28fOPjr9H5exZc9BLj/7NBZv3gsonVrUK7Ke6atS/JZwF2/ek/slXhxV5aFxS2hapwZzh15e6vXEbU3jaGY2XVrmfV3rUvaTnHaYHm0blbrOYPNLH72IRABRwAK36BERWS4io0WkbhHLDBGRWBGJTU31X39bY/Hfj5+S0w4XyPQ5raXCEgzAZ3M3kel+GSTs3M9FMb/zyFdLyc5W9h1yEsC+IhJBzoe/tF6evJoJS5O5/r/zCky774tYXpgYz+fzEr3Wk5MXdx04Uux8ew452yA9o/DXoap8tWBLka8zv0NHM/nnTyuYvsppnd86aj798h3mL92yl7827Oa2j+fz6DdxHDxa+PaH46foPQ84du53unFK+3uI6z6Yy62j5uc+v/2TBfR9bw4T45LZllZ4AitJMsoxYWkSb/62NrflXFLX/3ceA0b+lafsaKaz3+1Iz2DSsm08PzGeRYnOEe7ouZsK7eYqyiF3OyXsPJCb5AGuePtPBn8eC8CNI+Zx44jjMUyMS2bs/M2s8+jyu++LWJ6fGF/sulZvT2fm2p1eY7pxxF/cUMg+Ds4+9/QPy/gtfkduWXIR7483/T+cy80fHX9dB45k8uX8zfR6Zxb3fLaII5lZJOzcz7RVBY8m80s7dJSvFmxh+qqU3K7Qo5nZefJEefB5zFgRqQmMBx5T1XQRGQG8jJMiXwbeAgbnX05VRwGjAKKjo/3WB+DP63C6xfxOl4jCWysPjl3C1/dfSNcz6+cp/9fPq1i/8wBnNqzJy5NX5ZavSN6XG9ykZdvoe37jAi2snC+RxZv3cuRYFhed1YCd+zN4Z9o62jerQ81qVbi2QxPAaal/Pi+Ru7pGsLuYxDxzrfMlmn74GIm7DrJyW8HD7+ISwENjF/PhbR2pVMkJ/uCRTD6bm5g7fcy8RM6ofzLVq1TO3RYrkvfx7IQVzFy7k1F3RRda7+GjWXy1cAv3XBRBzJQ1jJ2/hbHzt+Rp4WYcy6J61coAuV9ip5zkPM/5jHwXuxUFup1Vn8a1awDHT93kvK64rWm5CQ+ck6Y704/Q42ynZbZ6ezrJew+TeuAI61MO8MK1Tg+k528qNqYeYE7C8a65R7+Jo2mdGjxwWSv6RzXl1OpVc6dVEvhhcRIXn9WA02tXJ2Hnfmav38Wp1atyY6dmpKQfP3fw+LfLAPjgjwTeHNCBk0+qTNvTa3Fmw5qFvyFF2HPwKGPnb87dbjvS856f+HxeIv0imxQ44jt8NIsP/0igcZ3q3H5Bi9zy0t7oL+NYFo9+E5f7vLgjlanxO2hR/xTanl4LyHs0vDwpjb2HjnFZm4YlXveeg0fp+PI0AL6LTeLLe7sUOe/CTXtQVS5oVZ9Nuw4Sv20fV5/XmDF/JXJL5+a5X5aeXpi4kh+XHO+O++jPjbw9bR0Am/59NTvSM5ibsJubOjUrsOyT3y9j+urjX2JvDujArgNHiJmyhkoiDLooosSv0xc+JXoRqYqT5Mep6o8AqpriMf1jYLJPEZZSLTnMFZUWMz27k1/qW5hYdD/qwI/nF7pDf7VgS4Eyzxbq6u3pXPrGH/yz7zn0Pvf0PPO9MnkVn8zZBDg7UZdXZwDw9UKnf/nq8xvT482ZXNuhMR/+sYH9GZlM9WhZ7D14lLqnnFRg/Qp0d68kyrE+ZT9zE3bRosEpRb7GKSt3MHXVDk6qUomvFmylce3qudN+i9+RJ4H+/sRltGpYk/kbnaOqqatS+GT2RgZ3a8l9X8Qy6KIILmvTEFWlw0tTOZqZTcNa1fL0Ked8gABipqyhaZ0a3NnVIwG5WXxOgvMFtmDTHhZsct6jcfddQLvGp+YmvMWb97JmR3qek4CKc9IUjiej/CemH72iNZPikvO0RK98Z1aBcxLJaYd5YWI8L0yMZ/LfL84tP3Aki+cnxnNmw1OY8UT3PL/NyFblU/f9ze/J75flPk6M6cuM1Sl8u2grH93ZiZbDfuWffc+hZrUqed7fqfE7aNnglDznU3budxJJfqPnJvJs7eqMm7+FMxudwvVRzXj9tzW5X9yNalXPTXTermqJGPpLnufH8rVOr3l/Nj/9rVuB5aatSmHIl4sB+M+tkbkNEXCScE5LOue9mbN+F7Gb9xR7xU5Oks/heaJ2z8Gj1DvlJFSVa96fQ/w251zYpEe65R5BvzUgm3/9vIrf4ndw8knHU+KAkfO4qVOzPEkenO67HC2HHT8F2ee80zm5amXuHL2A+y5pxeRl2/MkeXDe4+gWzpftmh3ld7GDlOZwLs+CziduDLBHVR/zKG/s9t8jIo8DF6jqrcXVFR0drbGxsaUPIv4n+H5QoZMiMr4qfX1l8EiPs6hVvQr/LuSD5av7L2nJx7MLTwrF6XhGHdIOH+PBy87k6R+ccXU7NK/DsiI+LO/eEslj38YVOg3g7NNr5e6U3c6qz9yEorvH7r24ZYFEVv+Uk9jtJvPEmL4s25qW+8V3T7cIZq1LZUPqwSLrvK5DEyYt21bk9NL4YnAX7hrtJIJHepxFzepVCk2K/vLq9efx3ISVpV5uwt8uKrQrzp9ev6k9n8zeyLqUAz7Vs+nfV3PlO7NI2OlbPZ42vnY1n87ZxKteLpFtXq8GW/cU30Uz7KqzaV7vZP42bonf4ivMiuG9uG9MbG4dFbGHAAASqElEQVTDoyTKeo4mh4gsVtXCD5s95/Mh0V8MzAZWQO7PUp8FBgKROI2nROCBnMRflEAk+seO/o14jWC9FjycMsGz7pWraPPPKcEOw/jR2Hsv4I5PF3if0RTwdJ+2dG/TiHZNTi3T8gFP9P5U5kS/aiJ8d1exs5RXy94YY8qqrC37kib6kPllrDHGmMJV7ETfqkewIzDGmBNexU701cvWr2WMMeGkYid6Y4wxXoV8or+j8jTvMxljTAgL+UT/StXPgh2CMcYEVcgnemOMCXdhkuiD/1sBY4wJlrBI9L0rLQp2CMYYEzRhkeg/OuldxA+DhxtjTEUUFoke4Ikq3wc7BGOMKeCjO/1zp93ihE2if6TKRFrI8UEJLmndwC/1eo4UFQgNax0f7enxK9qUevm2p9UqctoDl7YqUR2dWhQ6dkwe55bxpkzerHm5T0DqzfFxEffLv7ZDE+JeuDL3+b+uOxeAy88u++hCNdx76xfmrEYF7z9/TuPA/yDwzIbHb1Hdq91pXuf33J/eGtCh0HnaN6vN6LvzbtfTTq1W4LNyQ8fjA9INcO/l3j+ySZHrvq5D0dMC5dmrzw74OvLfqjwQwibRA/xZ7R/0i2zC2afX4uO7ork+qiknn5T3w3eSO9ze2afXYsTtHWnZ4BSquINuVKuSd3N9dk9nXrv+fP5zaySfdE7mvsq/8P7AKGY8cRmDurbg+WvaMaqYb+t+kU1YPrwXF5/lfOlc26EJ913cMnf6fRe3ZOGzPXmqd1u+uv8CHr2idYE6/n3D+Xmezxt6OXde2IKZT3YnMaYvEx85fk/w9wdG5Zl36FVn88ClrZj894tZ8GxPboluzrpXrsod3CNH5UrCoz1b08/jQzjx4W6c37Q217RvTGJMXyb//WIuaFmPHm0bMqhrC5rWqcHQqwp+SB7pcZZzq+IXeuWWPXjZmQzo1IxFz12Rm0g/HRRNYkxfqlWpxIOXnZk77z/7nsNnd3fOfX5Tp2asfqlPnptC/e+xS4r9Ik+M6csXg7sw9KqzubLdadSqXoXzmp6aJ4m9PzCKOiefxLNXn83n93Rm0EURJMb0ZfTdnXmmT9Ef/rWvOLG80v+8POUv9z+Phc/1zDM4Rc7gGh2a1ea3xy6lQ/M6nN/UGaJw3tDLmfLoJbQ5zfsAJAue7UnzejXylA296mw2vnY1g7q2YMYTl/FwD2cbDuxyPNnOeaYHM57oTq3qzj3Y/9GrDYO7taQoNapW5t1bIwHn83Fjp2aMvKNjnnkSY/oy6ZGLqVwp72dl5B2deO368znbHWzkt8cu5a0BHYiofzIDuzTnjQEdSIzpy7u3RvHtkAvpeMbx4R7fGxhFYkxf3su3/3rq2qo+lUoxVsojPc7K8zznM+j5ZfzZ3Z0ZcumZrHm5DwM6NeOebhF8dk/nPMslxvRl2uOX5imbP6wn93SL4NGeeT+v/7jSaahFebw2z305oFQ16H+dOnXSMnvx1LL9/fy4qqpmHMvUFs9M1v9MX6ev/rJKs7Ozi1xVdna2/mtSvK5P2V90HMV4e+paXbx5T4HyI8eydOj45ZqSflizs7P1pZ/jdX1KeqF1vD9jnS7ctFvnJezS//6RoKqq3y3aoi2emawf/L6+2GVyjJu/Wf+3cnuxsX75V6JOWbFN/zlhhW7dc1BVVQ8dcbbVZa//XuyyhZmyYpuOm785T1n64aP69PfL9EDGMa/Lj56zUX9fk6KaFKv68+P64e/rdF7CrjzzeG4TVdXkvYf0wS9j9cnv4jRuy1697PXf9asFm/NXnceEJUn645KtXuP5IXar/rQ0KXe9t3w0T5eOeVJ1zZQ8842cmaBz1qfmKZu7PlVHzkzQkkhJP6xDxy/TI8eyVFX1nWlrtedbM3X19n36zA/L9PDRTFVVXZ+yX1/+OV4/m7NRZ6zeUaCefcVs64Sd+/Vfk+Jz9/0pK7bruPmb9Z1pazU2cY8On7RSN+x09vmsrGx9ceJK3ZR6IHf5tTvS9Yq3ZuroORtzy45lZumzPy7XVybHa6thv+TWvTH1gL44caVmZRX9Ocuxbke6vvxzfJ7PZNyWvfp/U1Zri2cm5/499X2cqqpu2Llf/9+wofrVu8/kzp+Zla3P/7RCV23bp09/v0z3u68/MytbbxoxV8974X954lZV3ZZ2SIf9uFyPZmYVGteCjbv15pHz9JPZGwud7untqWv1k9kb9T/T1+nRzCwd9uNy3bHvsI6YmaBzE1K9Lu8NEKslyLEV+zbFAMOLHqTZq0btoOVl0OIiaHcd7EqA31+GGz6GKgVHaSpRHMPdofomPAjLvj7+vDiqMOnv0PEuaO4Og7biB0jbDAd3QetecKYfbuCWnQ0TH4Yu90HTTrB6MiTHwhXDYeo/ITsLEOjz2vFlfn0K2vWDiIuLqNTDvmRn/htGQbVCWqJrp8CWv+DKl/ItlwRTnnGWO6no0a4AeK0pHD0AQ7c69zraPA+WfwvX/gcO7na2Y78P4OQiBqzO2Oe8N9e8C7W8d1WUyM+Pwvk3w+dXO89L8p7nxpPuxvM21Drdef7t7XAsAwZ+DacUcmSSdQx+HAKXPQONPI4ujhyAnx6Eq16HU92jrx0rYe670H8kVPZ55FDvfn0KFo6CJxOgZjHDAWZnwU8PQdeHoXHhXUDF2jTLGY/imrdh52r474Xw/5bCe26rvzTvAcD04XBGV2jTu/SxBFHQ70cvIn2A/wCVgU9UNaaoeYOW6D29mAafXglJ7qWYDy+Ejy5zPkgn14cGbaBuS5jylDP95i+cBKjq7LDLvnbK750Oc96Bte5Qaw/Ng7nvQfYxOP18iP0MpJLzYU1Pcuo+5DFiU71WsGdjwfgufNgZDLVWY2cnH/AZLP8OjqTDOdfC/4Y5X1ynNoHFn0NmBgz5M2/CjZ8A399dsO5HFsMHHl1M174HP/8/OLMnbHCGMuT8AbDie3h6k5NEjx6CH++H3q9C3Qhnnh8fgOXfHH8dD82DV0+H2s1h39bj9Q/f52y3CQ848c55xym/4WNof7PzOCkWPukJlU+CW7+CZd84XwQxLeDofmjd2/ny+99QZ/6r3jj+3uS4axLM/6/z5ZKxD2rUhcPHhz7kjK4w+H95l5n9NtSoA9GDIWEGjL0BbvwUzr8Jti+Dj9zD9E73OF+Wra+Et9rmrWP4PtgW5yTYGz91vrQnDHFeX2YG7E+BP15x39e/OTEC1KgHh/ONTnTzl5AS72zXvYlO2aDJMOYaqF4bhnoMWzn+Puc9atQO+o+Avz6A5CWwZ4PzpRD3lfM+/H0JpK6BrQsg6i7nS37A51C1Omya7Wzr/h9C0mKY8rTzfg/4/PiX8Lz3nWWe3wWVqzr7wvd3w1X/B+9F5o2/5wtwyRPwr3qgWc6X0JSnoUlH2LYE6rSAXq84j68Y7uwL04c7y14xHBCnwXNoD/T70NnnrnwJPog+vq0LywFnXeFs+xpuN8nhNHj7HKdh16o7ZB523qP0bU6MY65x5hvw+fHPyLPbnNf8QWfYtc7Z59b/5sSROBe2LYXU1c5rqd0UbvrM+Vz/cI+zLfZshC+vd+pqdC7sjIcWF8ONn8Db7hd0u/5w02ioVPT5m+IENdGLSGVgHXAlkAQsAgaq6qrC5j8hEr0xxgRD/xEQeVuZFg32wCNdgARV3aiqR4FvgH4BWpcxxlRcPz0U8FUEKtE3BTyO1Ulyy/zv+o8CUq0xxoSKQCX6wi50ytNHJCJDRCRWRGJTU1PLvqYOtzp91MYYUxH1fDHgqwjUafgkoLnH82bANs8ZVHUUMAqcPnqf1nbLWJ8WN8aYUBaoFv0ioLWItBSRk4BbgUkBWpcxxphiBKRFr6qZIvII8BvO5ZWjVTU+EOsyxhhTvID9gkJVfwV+DVT9xhhjSias7nVjjDHhyBK9McaEOEv0xhgT4izRG2NMiLNEb4wxIe6EuE2xiKQCm32oogGwy0/h+JPFVToWV+lYXKUTinG1UNVi7gftOCESva9EJLYkd3ArbxZX6VhcpWNxlU44x2VdN8YYE+Is0RtjTIgLlUQ/KtgBFMHiKh2Lq3QsrtIJ27hCoo/eGGNM0UKlRW+MMaYIFTrRi0gfEVkrIgkiMrQc1tdcRP4QkdUiEi8ij7rlw0UkWUTi3L+rPZYZ5sa3VkR6e5T7NXYRSRSRFe76Y92yeiIyTUTWu//ruuUiIu+5614uIh096hnkzr9eRAb5GFNbj20SJyLpIvJYMLaXiIwWkZ0istKjzG/bR0Q6uds/wV22sMF3ShrXGyKyxl33BBGp45ZHiMhhj+020tv6i3qNZYzLb++bOLcwX+DG9a04tzMva1zfesSUKCJxQdheReWGoO9jAKhqhfzDuf3xBqAVcBKwDGgX4HU2Bjq6j2vhDIDeDhgOPFnI/O3cuKoBLd14KwcidiARaJCv7HVgqPt4KPB/7uOrgSk4I4FdCCxwy+sBG93/dd3Hdf34fu0AWgRjewGXAh2BlYHYPsBCoKu7zBTgKh/i6gVUcR//n0dcEZ7z5aun0PUX9RrLGJff3jfgO+BW9/FI4KGyxpVv+lvAC0HYXkXlhqDvY6paoVv05T4AuapuV9Ul7uP9wGqKHwu3H/CNqh5R1U1Aght3ecXeDxjjPh4D9Pco/0Id84E6ItIY6A1MU9U9qroXmAb08VMsPYENqlrcD+MCtr1UdRawp5D1+bx93Gmnqupf6nwiv/Coq9RxqepUVc10n87HGaGtSF7WX9RrLHVcxSjV++a2RC8HfvBnXG69NwNfF1dHgLZXUbkh6PsYVOyum/IbgLwQIhIBRAEL3KJH3EOw0R6He0XFGIjYFZgqIotFZIhbdpqqbgdnRwQaBSGuHLeS9wMY7O0F/ts+Td3H/o4PYDBO6y1HSxFZKiJ/isglHvEWtf6iXmNZ+eN9qw+keXyZ+Wt7XQKkqOp6j7Jy3175csMJsY9V5ETvdQDygK1YpCYwHnhMVdOBEcCZQCSwHefwsbgYAxF7N1XtCFwFPCwilxYzb3nGhdv/eh3wvVt0Imyv4pQ2jkBtt+eATGCcW7QdOENVo4B/AF+JyKmBWn8h/PW+BSregeRtTJT79iokNxQ5axExBGSbVeRE73UA8kAQkao4b+Q4Vf0RQFVTVDVLVbOBj3EOWYuL0e+xq+o29/9OYIIbQ4p7yJdzuLqzvONyXQUsUdUUN8agby+Xv7ZPEnm7V3yOzz0Jdw1wu3uojts1stt9vBin/7uNl/UX9RpLzY/v2y6crooq+crLzK3rBuBbj3jLdXsVlhuKqa9897GSduafaH84wyBuxDn5k3Oi59wAr1Nw+sbezVfe2OPx4zj9lQDnkvck1UacE1R+jR04Bajl8XgeTt/6G+Q9EfS6+7gveU8ELdTjJ4I24ZwEqus+rueH7fYNcE+wtxf5Ts75c/sAi9x5c06UXe1DXH2AVUDDfPM1BCq7j1sByd7WX9RrLGNcfnvfcI7uPE/G/q2scXlssz+Dtb0oOjecGPuYrx/iYP7hnLleh/NN/Vw5rO9inMOl5UCc+3c18CWwwi2flO8D8Zwb31o8zpL7M3Z3J17m/sXn1IfTFzoDWO/+z9lhBPjQXfcKINqjrsE4J9MS8EjOPsR2MrAbqO1RVu7bC+eQfjtwDKd1dK8/tw8QDax0l/kA98eIZYwrAaefNmcfG+nOe6P7/i4DlgDXelt/Ua+xjHH57X1z99mF7mv9HqhW1rjc8s+BB/PNW57bq6jcEPR9TFXtl7HGGBPqKnIfvTHGmBKwRG+MMSHOEr0xxoQ4S/TGGBPiLNEbY0yIs0RvjI9EpLuITA52HMYUxRK9McaEOEv0JmyIyB0istC9N/lHIlJZRA6IyFsiskREZohIQ3feSBGZL8fvCZ9zH/GzRGS6iCxzlznTrb6miPwgzn3kx5XqXuHGBJglehMWROQc4Bacm79FAlnA7Ti3jFiizg3h/gRedBf5AnhGVdvj/HIxp3wc8KGqdgAuwvmVJjh3K3wM5x7krYBuAX9RxpRQFe+zGBMSegKdgEVuY7sGzg2msjl+I6yxwI8iUhuoo6p/uuVjgO9FpBbQVFUnAKhqBoBb30JVTXKfx+Hcj2VO4F+WMd5ZojfhQoAxqjosT6HI8/nmK+6eIMV1xxzxeJyFfbbMCcS6bky4mAHcJCKNIHcszxY4n4Gb3HluA+ao6j5gr8dAFXfi3BkxHUgSkf5uHdVE5ORyfRXGlIG1OkxYUNVVIvJPnFG4KuHc/fBh4CBwrogsBvbh9OMDDAJGuol8I3CPW34n8JGIvOTWMaAcX4YxZWJ3rzRhTUQOqGrNYMdhTCBZ140xxoQ4a9EbY0yIsxa9McaEOEv0xhgT4izRG2NMiLNEb4wxIc4SvTHGhDhL9MYYE+L+P+6OoJGEQ0uBAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "run_experiment(env, agent_Q, n_experiments=20_000, max_steps=200, render=False, n_epoch_update=10_000, plot_stats=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The average reward should have increased and the average number of steps should have decreased. We can now measure the performance of the agent after learning by averaging the performance over 10 experiments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+---------+\n",
      "|R: | : :\u001b[35m\u001b[42mG\u001b[0m\u001b[0m|\n",
      "| : : : : |\n",
      "| : : : : |\n",
      "| | : | : |\n",
      "|Y| : |B: |\n",
      "+---------+\n",
      "  (Dropoff)\n",
      "\n",
      "Average reward       : 6.8\n",
      "Average #penalties   : 0.0\n",
      "Average #steps       : 14.2\n",
      "Average #reward/step : 0.5125678733031674\n"
     ]
    }
   ],
   "source": [
    "# calculate performance of agent\n",
    "agent_Q.learn = False\n",
    "run_experiment(env, agent_Q, n_experiments=10, max_steps=200, render=True, sleep=0.1)\n",
    "agent_Q.learn = True"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to go further from here?\n",
    "This notebook provides a basic processing pipeline for building a reinforcement learning agent that learns to carry out a job. Experiment a little bit yourself by changing the exploration parameter epsilon and discount factor gamma. Try to increase the reward or shorten the training time."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
